#!/usr/bin/env python
# coding: utf-8

import psycopg2
import requests
import numpy as np
import pandas as pd
import pandas.io.sql as psql
import time
import os
import copy


def download(url, file_name):
    downloaded = False
    while not downloaded:
        try:
            response = requests.get(url)
            if not os.path.isfile(file_name):
                with open(file_name, 'wb') as file:
                    file.write(response.content)
            downloaded = True
        except requests.exceptions.ConnectionError:
            time.sleep(1)


def download_wps_data(save_path, tests=None, wafers=None, save_header=True, download_data=True, save_results=True):
    conn = psycopg2.connect(
        host='effect-postgres.postgres.database.azure.com',
        port=5432,
        dbname='effect_core_v_4',
        user='tm@effect-postgres',
        password='9TWsxFMqsSkmL9ZZ'
    )

    # create directory to save data
    if not os.path.isdir(save_path):
        os.makedirs(save_path)

    measurement_result = 'chip_measurement_result'
    name = 'chip_name'
    session_id = 'chip_meas_session_id'
    design_id = 'chip_design_id'
    design_name = 'chip_design_name'

    if save_results:
        query = f"select mr.id, mr.chip_meas_session_id, mr.raw_result_ref, mr.grade, mr.analyzed_results,\n"
    else:
        query = f"select mr.id, mr.chip_meas_session_id, mr.raw_result_ref, mr.grade,\n"
    query += f"c.name as {name}, cd.name as {design_name}, cp.x_location as x_position, cp.y_location as y_position,\n"
    query += f"w.name as wafer\n"
    query += f"from {measurement_result} mr\n"
    query += f"inner join measurement_session ms on mr.{session_id} = ms.id\n"
    query += "inner join chip c on ms.chip_id = c.id\n"
    query += "inner join chip_position cp on c.chip_position_id = cp.id\n"
    query += "inner join chip_design cd on cp.chipdesign_id = cd.id\n"
    query += "inner join device d on d.chip_id = c.id\n"
    query += "inner join wafer w on d.wafer_id = w.id\n"

    if wafers is not None or tests is not None:
        filter_query = "where \n("

        if wafers is not None:

            for i, w in enumerate(wafers):
                if i != 0:
                    filter_query += " "
                filter_query += f"w.name = '{w}'"
                if i < len(wafers) - 1:
                    filter_query += "\n or"
            filter_query += ")"
            query = query + filter_query

        if tests is not None:
            if wafers is not None:
                filter_query = " and \n("

            for i, t in enumerate(tests):
                if i != 0:
                    filter_query += " "
                filter_query += f"mr.measurement_type = '{t}'"
                if i < len(tests) - 1:
                    filter_query += "\n or"
            filter_query += ")"
            query = query + filter_query

    # create a dataframe
    df = psql.read_sql(query, conn)
    df.reset_index(drop=True, inplace=True)

    # create a header file per wafer
    if save_header:
        for wafer in wafers:
            header_file_name = f"{wafer}__HEADER_FILE.txt"
            df.loc[df["wafer"] == wafer, :].to_csv(os.path.join(save_path, header_file_name))

    if download_data:
        for i in range(0, len(df)):
            url = 'https://effectbi.blob.core.windows.net/meas-raw/' + df.loc[i, 'raw_result_ref']
            name = str(df.loc[i, 'raw_result_ref']).split('/')[-1]
            if not os.path.exists(os.path.join(save_path, name)):
                download(url, os.path.join(save_path, name))


if __name__ == '__main__':

    tests = ['GAIN_DIODE_TEST']  # CHANGE TESTS
    # wafers = ['12845304962S', '12845305162S' '12845305262S', '12845305362S', '12845305462S', '12845305562S', '12845305662S', '12845305762S',
    #           '12845305962S', '12845306062S', '12845306162S', '12845306262S', '12845306362S', '12845306462S', '12845306562S',
    #           '12845306762S', '12845306862S', '12845306962S', '12934101562S', '12934101762S', '12934101862S', '12934101962S',
    #           '12934102262S', '12934102462S', '12934102662S', '12934102762S']  # CHANGE WAFER NAME
    wafers = ['13755000862S']
    folder_path = r"C:\data\WPS\Cuttlefish1a"  # CHANGE DIRECTORY PATH TO SAVE DATA

    for wafer in wafers:
        print(wafer)
        save_path = os.path.join(folder_path, wafer)
        download_wps_data(save_path, tests, [wafer], save_header=True, download_data=False, save_results=False)

    # osa = 'See Exact BOM'
    # # osa = '620-00002'
    # process = 'Post Fibre Alignment Test_Primary'
    # save_path = r'C:\Users\DiogoMotta\Desktop\data\OSA\{:}'.format(osa)  # CHANGE DIRECTORY PATH TO SAVE DATA
    # tests = ['FIBRE_COUPLING_TEST', 'MODULATOR_TEST', 'MGDBR_TUNING_TEST_LIVE']  # CHANGE TESTS
    # save_header = False  # header file contains grading information, set this to false if you don't want this information
    # download_osa_data(osa, save_path, tests, process)
